#testing
from subprocess import Popen, PIPE
import re

insertStatement=""
#function that takes the sqlCommand and connectString and returns the queryReslut and errorMessage (if any)
def runSqlQuery(sqlCommand, connectString):
   session = Popen(['sqlplus', '-S', connectString], stdin=PIPE, stdout=PIPE, stderr=PIPE)
   session.stdin.write(sqlCommand)
   return session.communicate()

#function to write the error messagae to a log starting at the Error and line#
def printError(result):
	message = str(result)
	regexed = re.sub(r'\\(?<=\\)(r|n)\s*|(?<=\s|\()b|''', "", message)
	regexed = regexed.split("ERROR",1)
	errorMessage = "*ERROR*" + regexed[1] + regexed[0]
	with open("ErrorLog.txt", "a") as error:
		error.write(errorMessage + "\n")

with open("Oracle2.csv") as f:
	for line in f:
		line = line.split(',')

	 # tediously assign values to variables for the sake of readability
	 # split command to rid CR and LF
		id = line[0].strip()
		seq = line[1].strip()
		act_ts = line[2].strip()
		ucq_type = line[3].strip()
		system = line[4].strip()
		operation = line[5].strip()
		num_items = line[6].strip()
		result_code = line[7].strip()
		advisory_code = line[8].strip()
		op_size = line[9].strip()
		main_c_size = line[10].strip()
		top_op_size = line[11].strip()
		top_c_size = line[12].strip()
		top_latency = line[13].strip()
		bottom_latency = line[14].strip()

		insertStatement += "insert into GEOFFREY2.SNACKS (ID, SEQ, ACT_TS, UCQ_TYPE, SYSTEM, OPERATION, NUM_ITEMS, RESULT_CODE, ADVISORY_CODE, MAIN_C_SIZE, TOP_C_SIZE, TOP_LATENCY, BOTTOM_LATENCY) \
		VALUES ('" + id + "', '" +  seq + "', " + "TIMESTAMP '" + act_ts + "', '" + ucq_type + "', '" +  system + "', '" + operation + "', '" + num_items + "', '" + result_code + "', '" + advisory_code + "', '" \
		+ main_c_size + "', '" + top_c_size + "', '" + top_latency + "', '" + bottom_latency + "');\n"

# print(insertStatement)
connectString = 'SYSTEM/Tubes12!'
sqlCommand = insertStatement.encode(encoding='UTF-8')
queryResult = runSqlQuery(sqlCommand, connectString)

# if "ERROR" in str(queryResult):
# 	printError(queryResult)

with open("ErrorLog.txt", "a") as error:
		error.write(insertStatement)
print("done")		