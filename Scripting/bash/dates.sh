#!/bin/bash
# GNU bash, version 4.3.46

#initialize 'today' to current date
today=$(date)
echo "Today is $today"

#check if yesterday is Saturday
if [ $(date --date "$today" '+%u') -eq 7 ]
then
lstWk=$(date -d last-sunday)
fullLstWkStr="$(date --date "$lstWk - 7days" '+%Y/%m/%d')"
fullLstWkEnd="$(date --date "$fullLstWkStr + 6days" '+%Y/%m/%d')"
echo "Beginning of last week, $fullLstWkStr"
echo "End of last week, $fullLstWkEnd"
twoWkStr=$(date --date "$fullLstWkStr - 7 days" '+%Y/%m/%d')
twoWkEnd=$(date --date "$fullLstWkEnd - 7 days" '+%Y/%m/%d')
echo "Two Weeks ago, Beginning, $twoWkStr"
echo "Two Weeks ago, End, $twoWkEnd"
fi

#Check if yesterday is end of Month
if [ $(date --date "$today" '+%d') -eq 01 ]
then 
lstMth=$(date -d "-1 month -$(($(date +%d)-1)) days" '+%Y/%m/%d')
lstMthEnd=$(date -d "-$(date +%d) days" '+%Y/%m/%d')
twoMthStr=$(date --d "-2 month -$(($(date +%d)-1)) days" '+%Y/%m/%d')
twoMthEnd=$(date --d "-$(date +%d) days -1 month" '+%Y/%m/%d')
echo "Last Month, Beginning, $lstMth"
echo "Last Month, End, $lstMthEnd"
echo "Two Months, Beginning, $twoMthStr"
echo "Two Months, End, $twoMthEnd"
fi

#Check if yesterday is end of year / first quarter
monthDay=$(date --date "$today" '+%m%d')
if [ $monthDay -eq 0101 ]
then
lastYear=$(date --date last-year '+%Y')
echo "First day of new year, last year was: $lastYear"
echo "First day of New Quarter: First Quarter"
fi

#second quarter
if [ $monthDay -eq 0401 ]
then 
echo "First day of New Quarter: Second Quarter"
fi

#third quarter
if [ $monthDay -eq 0701 ] 
then
echo "First day of New Quarter: Third Quarter"
fi

#fourth quarter
if [ $monthDay -eq 1001 ] 
then
echo "First day of New Quarter: Fourth Quarter"
fi

$SHELL