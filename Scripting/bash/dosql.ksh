#!/bin/ksh

# script to do a generic Oracle SQL statement via sqlplus

# use std input for the actual queries, no end of statment required.
# use script in line args for sqlplus args.
# output will automatically be formatted.

ARGS=""
TERMINATESQL="yes"
INCLHEADER="no"
IGNOREDNE="no"
while test "$1" != ""
do
   if test "$1" = "-noterm"
   then
       TERMINATESQL="no"
       shift
       continue
   fi
   if test "$1" = "-inclhdr"
   then
       INCLHEADER="yes"
       shift
       continue
   fi
   if test "$1" = "-ignoredne"
   then
       IGNOREDNE="yes"
       shift
       continue
   fi
   ARGS="$ARGS $1"
   shift 
done

umask 0002

SQLIN=/tmp/sqlin.$$
test -f $SQLIN && rm -f $SQLIN
SQLOUT=/tmp/sqlout.$$
SQLOUT2=/tmp/sqlout2.$$
trap "rm -f /tmp/sqlin.$$ /tmp/sqlout.$$ /tmp/sqlout2.$$" 0 1 2 3 15

# stdin is a file, so read args from this file
if test $INCLHEADER = "yes"
then
    echo -e "set pages 50000\nset wrap on\nset trim on" > $SQLIN
else
    echo -e "set pages 0\nset wrap off\nset trim on" > $SQLIN
fi
cat >> $SQLIN
# add "/" just in case not there.
test "$TERMINATESQL" = "yes" && echo -e "/\nexit" >> $SQLIN

sqlplus -s $ARGS @$SQLIN > $SQLOUT 2>&1
if test $? -ne 0
then
    # sql error during execution
    echo -e "\n*** Error during query execution *** \n" >&2
    cat $SQLOUT >&2
    rm -f /tmp/sql*.$$
    exit 2
else
    # query worked, so parse results
    cp $SQLOUT $SQLOUT2
fi

grep "^no rows selected$" $SQLOUT2 > /dev/null 2>&1
if test $? -eq 0
then
    rm -f /tmp/sql*.$$
    exit 1
fi

# concatenate error msgs
awk '{if (substr($0,1,14)=="ERROR at line ") {
         printf("%s ",$0)
      } else {
         printf("%s\n",$0)
      }}' $SQLOUT2 > $SQLOUT

if test "$IGNOREDNE" = "yes"
then
    grep -v "ORA-.*does not exist" $SQLOUT > $SQLOUT2
    cp $SQLOUT2 $SQLOUT
fi

grep "ERROR: ORA" $SQLOUT > /dev/null 2>&1
if test $? -eq 0
then
    echo -e "\nError during SQL execution:\n"
    cat $SQLOUT >&2
    rm -f /tmp/sql*.$$
    exit 2
fi

grep "ERROR at line" $SQLOUT > /dev/null 2>&1
if test $? -eq 0
then
    echo -e "\nError during SQL execution:\n"
    cat $SQLOUT >&2
    rm -f /tmp/sql*.$$
    exit 2
fi

grep "^SP2-[0-9]+:" $SQLOUT > /dev/null 2>&1
if test $? -eq 0
then
    echo -e "\nError during SQL execution:\n"
    cat $SQLOUT >&2
    rm -f /tmp/sql*.$$
    exit 2
fi
grep "^unknown command beginning" $SQLOUT > /dev/null 2>&1
if test $? -eq 0
then
    echo -e "\nError during SQL execution:\n"
    cat $SQLOUT >&2
    rm -f /tmp/sql*.$$
    exit 2
fi


grep -v "Nothing in SQL buffer to run." $SQLOUT

rm -f /tmp/sql*.$$
exit 0


