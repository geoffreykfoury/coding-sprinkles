#!/bin/ksh

# read query from std in
sqlquery=`cat`

cat << EOF | dosql.ksh $* > /tmp/sqlout.$$
set colsep |
set linesize 3000
set tab off
$sqlquery
EOF

status=$?
if test $status -eq 0
then
    grep "invalid username/password;" /tmp/sqlout*.$$ > /dev/null 2>&1
    if test $? -eq 0
    then
        echo "Error! Invalid userid/password specified."
        rm -f /tmp/sqlout.$$
        exit 2
    fi 
    # query worked, display output
    cat /tmp/sqlout.$$ | awk '{
       if (length($0)==0) next
       if (($2=="rows")&&($3=="selected.")) next
       gsub(/[ ]*\|/,"|")
       gsub(/^[ ]*/,"")
       gsub(/\| */,"|")
       print $0
    }'
    rm -f /tmp/sqlout*.$$
    exit 0
fi

if test $status -eq 1
then
    # query returned no rows
    rm -f /tmp/sqlout*.$$
    exit 1
fi
   
# otherwise some error
cat /tmp/sqlout*.$$ >&2
exit 2 
 
