Coding examples that may provide reference for particular tasks

## **Java** ##

1. **Filer**: This sprinkle is a layout for reading directories and files while locating a string/pattern, and saving a certain amount of bytes/chars before & after the located character sequence (called snippets).  User can find the string in tens of thousands of files and have 200 words before and after found word for context. Tested on a directory with over 10,000 folders and 20,000 documents.

2. **JMX-Camel**:  This is a quick manual way to have camel system and JVM metrics be shown in JMX.

## **Javascript** ##

1. **Slow down loop**: Slowing down a loop in javascript is not trivial.  This allows for the loop to slow down so other processing can complete and the logic within the loop does not outpace it.

## **DB** ##

1. **sqlPy.py**: This is a simple script that can migrate data information from a file into a SQL database.  This was originally created to migrate data from a local Oracle DB to a file, and then pipe data from file into a remote Oracle DB on AWS (RDS).

2. **OuterSelectUnion**: Quick refresher on using outerselect with SQL.

## **Scripting** ##

Cotains some pretty basic but reusable code snippets for Bash, Powershell, and Python