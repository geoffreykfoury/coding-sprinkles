<style>
body { margin: 0;}
canvas { width:100%; height:100%}
</style>

<body>
<script src="three.min.js"/>
<script src="OrbitControls.js"/>
<script src="Stats.js"/>

var stats = new Stats();

stats.setMode( 0 );

stats.domElement.style.position ='absolute';
stats.domElement.style.left = '0px';
stats.domElement.style.top = '0px';
document.body.appendChild(stats.domElement);



var update = function (){
stats.begin();
stats.end();
requestAnimationFrame(update);
};


requestAnimationFrame(update);

var clock = new THREE.Clock();
var scene = new THREE.Scene();
var object, myAnim;
var handler = THREE.AnimationHandler.CATMULLRON;
var camera = new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight, 0.1, 1000);

var webglRenderer = new THREE.webglRenderer();
webglRenderer.setSize(window.innerWidth, window.innerHeight);
webglRenderer.setClearColor(0xffffff);
document.body.appendChild(webglRenderer.domElement);
var controls = new THREE.OrbitControls(camera, webglRenderer.domElement);


var geometry = new THREE.PlaneGeometry(200,200);
var material = new THREE.MeshPhoneMaterial({map:
THREE.ImageUtils.loadTexture('textoload.png')});
plane.rotation.x=-90 * (Math.Pi/100);
plane.receiveShadow=true;
scene.add(new THREE.AmbientLight(0xffffff));
var light = new THREE.DirectionalLight(0xffffff, .4);
light.position.x = -100;
light.position.y = 50;
scene.add(light);


var spereGeo = new THREE.SphereGeometry(250 , 32 , 32);
var sphereMtl = new THREE.MeshBasicMaterial()
sphereMtl.map = THREE.ImageUtils.loadTexture('textoload.jpg')
sphereMtl.side = THREE.BackSide
var myShere = new THREE.Mesh(sphereGeo, sphereMtl);
scene.add(mySphere);


var loader = new THREE.JSONLoader();
loader.load("model.js", action);
animate();


function action(geometry, materials)
{
materials.forEach(function (mat){mat.skinning = true;});
object = new THREE.SkinnedMesh(geometry, new THREE.MeshFaceMaterial(materials));
object.castShadow=true;
myAnim = new THREE.Animation(object, geometry.animation, handler);
myAnim.play();
scene.add(object);
}


function animate(){
	requestAnimationFrame(animate);
	controls.update();
	var delta = clock.getDelta();
	THREE.AnimationHandler.update(delta);
	render();
}

function render(){

	webglRenderer.render(scene, camera);
}
</body>