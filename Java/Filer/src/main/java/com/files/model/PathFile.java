/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.files.model;

/**
 *
 * @author gkfoury
 */
public class PathFile {
    
    private String pathForPage;

    /**
     * @return the pathForPage
     */
    public String getPathForPage() {
        return pathForPage;
    }

    /**
     * @param pathForPage the pathForPage to set
     */
    public void setPathForPage(String pathForPage) {
        this.pathForPage = pathForPage;
    }
   
    
}
