/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.files.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author gkfoury
 */
@Service
public class FileRead {

    /**
     * @param args the command line arguments
     */
    private ArrayList<String> pathNames = new ArrayList();
    private HashMap<String, String> snippets = new HashMap<String, String>();

    public HashMap readFiles(String search, String dirPath) {

        snippets.clear();

        //Method to get all the files in main and sub folders
        listDirectory(dirPath);

        for (int i = 0; i < pathNames.size(); i++) {
            String path = pathNames.get(i);

            try (InputStream stream = new FileInputStream(path); BufferedReader br = new BufferedReader(new InputStreamReader(stream))) {

                Field chars = br.getClass().getDeclaredField("cb");
                chars.setAccessible(true);

                Field ff = br.getClass().getDeclaredField("nextChar");
                ff.setAccessible(true);

                String line;
                while ((line = br.readLine()) != null) {
                    if (line.contains(search)) {
                        //Getting through to Java Source Code - uh oh
                        char[] cb = (char[]) chars.get(br);
                        int nextChar = (Integer) ff.get(br);
                        char[] charBuffer = new char[300];
                        System.arraycopy(cb, nextChar - 200, charBuffer, 0, 300);
                        snippets.put(path, new String(charBuffer));

                    }
                }

            } catch (IOException | NoSuchFieldException | SecurityException | IllegalAccessException ex) {
                Logger.getLogger(FileRead.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return snippets;
    }
//        public HashMap readFiles(String search, String dirPath) {
//
//    snippets.clear();
//    InputStream stream = null;
//
//    //Method to get all the files in main and sub folders
//    listDirectory(dirPath);
//
//    for (int i = 0; i < pathNames.size(); i++) {
//
//        try {
//            stream = new FileInputStream(pathNames.get(i));
//            BufferedReader br = new BufferedReader(new    InputStreamReader(stream));
//
//            Field chars = br.getClass().getDeclaredField("cb");
//            chars.setAccessible(true);
//
//            Field f = br.getClass().getDeclaredField("nChars"); 
//            f.setAccessible(true);
//
//            Field ff = br.getClass().getDeclaredField("nextChar");
//            ff.setAccessible(true);
//
//            String line;
//            //not used
//            int lineNum = 0;
//            String path = pathNames.get(i);
//            while ((line = br.readLine()) != null) {
//
//                //Getting through to Java Source Code - uh oh
//
//                // int charCount = (int) f.get(br);
//                char[] cb = (char[]) chars.get(br);
//                int nextChar = (Integer) ff.get(br);
//
//                //not used
//                lineNum++;
//
//                if (line.contains(search)) {
//                    String lines = "";
//                    for (int t = nextChar - 200; t < nextChar + 100; t++) {
//                        lines += cb[t];
//                    }
//
//                    snippets.put(pathNames.get(i), lines);
//
//                }
//            }
//
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(FileRead.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(FileRead.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IllegalArgumentException ex) {
//            Logger.getLogger(FileRead.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            Logger.getLogger(FileRead.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (NoSuchFieldException ex) {
//            Logger.getLogger(FileRead.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (SecurityException ex) {
//            Logger.getLogger(FileRead.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//    return snippets;
//}
//        

    private void listDirectory(String dirPath) {
        File dir = new File(dirPath);
        File[] firstLevelFiles = dir.listFiles();
        if (firstLevelFiles != null && firstLevelFiles.length > 0) {
            for (File aFile : firstLevelFiles) {
                if (aFile.isFile()) {
                    pathNames.add(aFile.getAbsolutePath());
                }
                if (aFile.isDirectory()) {
                    listDirectory(aFile.getAbsolutePath());
                }
            }
        }
    }

}
