/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.files.controllers;

import com.files.service.FileRead;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author gkfoury
 */
@Controller
public class SearchController {

    private FileRead fileRead;

    @Autowired
    public SearchController(FileRead fileRead) {
        this.fileRead = fileRead;

    }

    @RequestMapping("/page")
    public String home() {
        return "fullpage";
    }

    @ResponseBody
    @RequestMapping(value = "/index/search")
    public String add(@RequestParam String search, @RequestParam String path) {
        fileRead.readFiles(search, path);
        return "checking";
    }

}
