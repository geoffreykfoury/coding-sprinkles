/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.files.controllers;

import com.files.model.Criteria;
import com.files.model.PathFile;
import com.files.service.FileRead;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author gkfoury
 */
@Controller
public class HomeController {

    private FileRead fileRead;

    @Autowired
    public HomeController(FileRead fileRead) {
        this.fileRead = fileRead;

    }

    @RequestMapping("/")
    public String home() {
        return "index";
    }

    @ResponseBody
    @RequestMapping(value = "search", method = RequestMethod.POST)
    public HashMap add(@RequestBody Criteria searchData) {

        Criteria searchTerms = new Criteria();

        searchTerms.setSearch(searchData.getSearch());
        searchTerms.setPath(searchData.getPath());

        HashMap<String, String> snippets = fileRead.readFiles(searchTerms.getSearch(), searchTerms.getPath());

        return snippets;
    }

//    @RequestMapping(value = "fullpage", method = RequestMethod.POST)
//    @ResponseBody
//    public String fullpage(@RequestBody String path) {
//
//        String paths = path;
//        
////        PathFile pathObj = new PathFile();
////        pathObj.setPathForPage(path.getPathForPage());
//
//        
//
//        return "index_1";
//
//    }

}
