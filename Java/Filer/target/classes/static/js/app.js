/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {


    $('#search').on('click', function (e) {
        $('#table').empty();
    });
    $('#create-search').on('click', function (e) {

        e.preventDefault();
        var searchData = JSON.stringify({
            search: $('#search').val(),
            path: $('#path-name').val()
        });
//        $('#modal-title').text("");
//        $('#portfolio1 tbody').empty();
//        ;
        $.ajax({
            type: 'POST',
            url: contextRoot + 'search',
            data: searchData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data) {

                $.each(data, function (key, value) {
                    $('#table').append("<div id='snippet' style='background-color:grey;'><a class='link' id='" + key + "' href='/index_1.html'>" + key + "</a></br><textarea rows='10' style='width:100%; background-color: #F5F5F5'>" + value + "</textarea></div>");
                });
            }
        });
    });

    $('#errors').on('click', function (e) {

        var searchData = $(this).attr('id');

        $.ajax({
            type: 'GET',
            url: contextRoot + "errors",
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }

        }).success(function (data, status) {

            $.each(data, function (index, data) {
                var errorRow = errorRow(data);

                $('#all-section').append($(errorRow));



            });
        });
    });

    function errorRow(data) {
        return "<div><p th:text='" + data + "'> 'error here' </p></div>";
    }


});