/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing123;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

/**
 *
 * @author gkfoury
 */
public class Testing123 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        HashMap jmxInfo = new HashMap();
        HashMap jmxRoutes = new HashMap();
        

        try {
            // TODO code application logic here

            JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://localhost:1099/jmxrmi/camel");
            JMXConnector jmxc = JMXConnectorFactory.connect(url);
            MBeanServerConnection server = jmxc.getMBeanServerConnection();

            ObjectName objName = new ObjectName("org.apache.camel:type=routes,*");
            List<ObjectName> cacheList = new LinkedList(server.queryNames(objName, null));
            for (Iterator<ObjectName> iter = cacheList.iterator(); iter.hasNext();) {
                objName = iter.next();
                String keyProps = objName.getCanonicalKeyPropertyListString();
                ObjectName objectInfoName = new ObjectName("org.apache.camel:" + keyProps);
                String routeId = (String) server.getAttribute(objectInfoName, "RouteId");
                jmxInfo.put("RouteId", routeId);
                String description = (String) server.getAttribute(objectInfoName, "Description");
                jmxInfo.put("Description", description);
                String state = (String) server.getAttribute(objectInfoName, "State");
                jmxInfo.put("State", state);
                String meanProcess = server.getAttribute(objectInfoName, "MeanProcessingTime").toString();
                jmxInfo.put("MeanProcessingTime", meanProcess);
                String minProcess = server.getAttribute(objectInfoName, "MinProcessingTime").toString();
                jmxInfo.put("MinProcessingTime", minProcess);
                String maxProcess = server.getAttribute(objectInfoName, "MaxProcessingTime").toString();
                jmxInfo.put("MaxProcessingTime", maxProcess);
                String manName = (String) server.getAttribute(objectInfoName, "CamelManagementName");
                jmxInfo.put("CamelManagementName", manName);
                String endpoint = (String) server.getAttribute(objectInfoName, "EndpointUri");
                jmxInfo.put("EndpointUri", endpoint);
                String exchTotal = server.getAttribute(objectInfoName, "ExchangesTotal").toString();
                jmxInfo.put("ExchangesTotal", exchTotal);
                String exchFailed = server.getAttribute(objectInfoName, "ExchangesFailed").toString();
                jmxInfo.put("ExchangesFailed", exchFailed);
                String failHandled = server.getAttribute(objectInfoName, "FailuresHandled").toString();
                jmxInfo.put("FailuresHandled", failHandled);
                String frExchTime = server.getAttribute(objectInfoName, "FirstExchangeCompletedTimestamp").toString();
                jmxInfo.put("FirstExchangeCompletedTimestamp", frExchTime);
                String lsProcTime = server.getAttribute(objectInfoName, "LastProcessingTime").toString();
                jmxInfo.put("LastProcessingTime", lsProcTime);
                String inflight = server.getAttribute(objectInfoName, "InflightExchanges").toString();
                jmxInfo.put("InflightExchanges", inflight);
                
                
                jmxRoutes.put(routeId, jmxInfo);

            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(Testing123.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | MalformedObjectNameException | MBeanException | AttributeNotFoundException | InstanceNotFoundException | ReflectionException ex) {
            Logger.getLogger(Testing123.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
